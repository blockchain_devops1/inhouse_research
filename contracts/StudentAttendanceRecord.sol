// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract StudentAttendanceRecord {
    uint public studentsCount = 0;

    // Model a Student
    struct Student {
        uint _id;
        uint studentNumber;
        string name;
        bool attendanceStatus;
    }
    // Store students
    mapping(uint => Student) public students;

    //Constructor for students
    constructor() {
        // createStudent( 1, "Cheki Lhamo");
    }

    //Create and add student to storage
    function createStudent(uint _studentNumber, string memory _name)
        public returns (Student memory){
        studentsCount++;
        students[studentsCount] = Student(studentsCount,_studentNumber, _name, false);
        // trigger create event
        emit createStudentEvent(studentsCount,_studentNumber, _name, false);
        return students[studentsCount];
    }

    //Change graduation status of student
    function markAttendanceStatus(uint _id)
        public returns (Student memory)
    {
        students[_id].attendanceStatus = true;
        // trigger create event
        emit markAttendanceStatusEvent(_id);
        return students[_id];
    }
    //Fetch student info from storage
    function findStudent(uint _id)
        public view returns (Student memory)
    {
        return students[_id];
    }

    // Events
    event createStudentEvent (
        uint _id,
        uint indexed studentNumber,
        string name,
        bool attendanceStatus
    );

    event markAttendanceStatusEvent (
        uint indexed studentNumber
    );
}