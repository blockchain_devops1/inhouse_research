// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;

//Step 2: change the base code to use Component
import React, { Component } from 'react'
//Step 3 : Import the ABI
import { STUDENT_RECORD_ABI, STUDENT_RECORD_ADDRESS } from './abi/config_studentRecord'
import Web3 from 'web3'
import './App.css';
import StudentRecord from './components/StudentRecord'
//We replace function with a component for better management of code
class App extends Component {
  //componentDidMount called once on the client after the initial render
  //when the client receives data from the server and before the data is displayed.
  componentDidMount() {
    if (!window.ethereum)
      throw new Error("No crypto wallet found. Please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }
  //create a web3 connection to using a provider, MetaMask to connect to
  //the ganache server and retrieve the first account
  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "http://localhost:8545")
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })

    //Step 4: load all the lists from the blockchain
    const studentRecord = new web3.eth.Contract(
    STUDENT_RECORD_ABI, STUDENT_RECORD_ADDRESS)
    //Keep the lists in the current state
    this.setState({ studentRecord })
    //Get the number of records for all list in the blockchain
    const studentCount = await studentRecord.methods.studentsCount().call()
    //Store this value in the current state as well
    this.setState({ studentCount })
    //Use an iteration to extract each record info and store
    //them in an array
    this.state.students=[]
    for (var i = 1; i <= studentCount; i++) {
      const student = await studentRecord.methods.students(i).call()
      this.setState({
        students: [...this.state.students, student]
      })
    }

  }
  //Initialise the variables stored in the state
  constructor(props) {
    super(props)
    this.state = {
      account: '',
      studentCount: 0,
      students: [],
      loading: true,
    }
    this.createStudent = this.createStudent.bind(this)
    this.markAttendance = this.markAttendance.bind(this)
  }
  createStudent(studentNumber,name) {
    this.setState({ loading: true })
    this.state.studentList.methods.createStudent(studentNumber,name)
    .send({ from: this.state.account })
    .once('receipt', (receipt) => {
      this.setState({ loading: false })
      this.loadBlockchainData()
    })
  }

  markAttendance(studentNumber) {
    this.setState({ loading: true })
    this.state.studentList.methods.markAttendance(studentNumber)
    .send({ from: this.state.account })
    .once('receipt', (receipt) => {
      this.setState({ loading: false })
      this.loadBlockchainData()
    })
  }


  //Display the first account
  render() {
    return (
      <div className="container">
      <h1>Student Attendance Record</h1>
      <p>Your account: {this.state.account}</p>
      <StudentRecord 
        createStudent={this.createStudent}
        markAttendance={this.markAttendance}
      />

        <ul id="studentList" className="list-unstyled">
          {
            //This gets the each student from the studentList
            //and pass them into a function that display the
            //details of the student
            this.state.students.map((student, key) => {
              return (
                <li className="list-group-item checkbox" key={key}>
                  <span className="name alert">{student._id} {student.name}</span>
                  <input
                    className="form-check-input"
                    type="checkbox"
                    name={student._id}
                    defaultChecked={student.attendanceStatus}
                    disabled={student.attendanceStatus}
                    ref={(input) => {
                    this.checkbox = input
                  }}
                  onClick={(event) => {this.markAttendance(event.currentTarget.name) }}
                  />

                  <label className="form-check-label" > Attended? </label>
                </li>
              )
            }
            )
            }
          </ul>
    </div>
    );
  }
}
export default App;
